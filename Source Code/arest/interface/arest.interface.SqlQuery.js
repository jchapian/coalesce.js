aRest.fn.core.structure.query.util.Entry = (function(){
/////////////////////////////////////////

	var CONSTANT = aRest.fn.core.constant;
	var UTIL = aRest.fn.core.util;
	var DATA = aRest.fn.data;
	var STRUCTURE = aRest.fn.core.structure;

/////////////////////////////////////////
	     var _interface = {};
/////////////////////////////////////////

_interface = function(){
	var chain = STRUCTURE.query.QueryChain();
	var self = STRUCTURE.query.ChainLink(chain);

	self.set(UTIL.object.methodToCaps({
				new:STRUCTURE.query.create.New(chain),
			}));

	console.log(chain,self);

	return self.get();
}

return _interface;
})();

//apply interface to prototype
aRest = aRest.fn.core.util.object.applyToFn(aRest, aRest.fn.core.structure.query.util.Entry());