aRest.fn.core.structure.query.ChainLink = (function(){
/////////////////////////////////////////

	var CONSTANT = aRest.fn.core.constant;
	var UTIL = aRest.fn.core.util;
	var DATA = aRest.fn.data;
	var STRUCTURE = aRest.fn.core.structure;

/////////////////////////////////////////
/////////////////////////////////////////

		//query modifier (EX: select.group || select.module rather than select(@groupName))
		var modifier = function(action, exceptionArray, NEXT_LINK){

				var modifier = function(type){
					return function(id, properties){
						action(type, id, properties);
						return NEXT_LINK;
					}
				}

				return  {
							component:modifier(CONSTANT.parse.prefix.component),
							group:modifier(CONSTANT.parse.prefix.group),
							module:modifier(CONSTANT.parse.prefix.module),
							container:modifier(CONSTANT.parse.prefix.container),
							template:modifier(CONSTANT.parse.prefix.template),
						}
		};
		var chainQueryFrame = function(CHAIN){
			return UTIL.object.methodToCaps({
				insert: STRUCTURE.query.scope.Insert(CHAIN)
			});
		}
		var chainAction = function(THIS_LINK,NEXT_LINK,CHAIN){
			var fn = {};
				fn.sort = function(){}
				fn.sort.asc = function(){}
				fn.sort.desc = function(){}

				fn.get = function(i){
					if(!i && i!=0)return CHAIN.result;
					if( (i>=0) && (CHAIN.result.length <= i) )return null;
					else return CHAIN.result[i];
				}
				fn.get.all = function(){return CHAIN.result;};

				fn.set = {};
				fn.set.attribute = function(key, value){
					for(var i in CHAIN.result){
						CHAIN.result[i].set.attribute(key,value);
					}

					return NEXT_LINK;
				}
			return fn;
		}

		var _interface = function(_chain){
			var NEXT_LINK = {};
			var THIS_LINK = {};
			var CHAIN = _chain;

			var fn = {};
				fn.next = {};

			//set functions
			//// //// //// ////
			//base
			fn.set = function(ob){UTIL.object.merge(THIS_LINK, ob);}
			fn.next.set = function(ob){UTIL.object.merge(NEXT_LINK, ob);}
			//modifiers
			fn.set.modifier = function(action,excludes){
				UTIL.object.merge(THIS_LINK, modifier(action,excludes,NEXT_LINK));
			}
			fn.next.set.modifier = function(action,excludes, newLink){ //must provide a new link for the modifier when applying to next link
				if(!newLink)newLink = NEXT_LINK;
				UTIL.object.merge(NEXT_LINK, modifier(action,excludes, newLink));
			}

			//action
			fn.set.actions = function(){
				UTIL.object.merge( THIS_LINK, chainAction(THIS_LINK,NEXT_LINK,CHAIN) );
			}
			fn.next.set.actions = function(){
				UTIL.object.merge( NEXT_LINK, chainAction(THIS_LINK,NEXT_LINK,CHAIN) );
			}
			
			//frame queries
			fn.set.frameQuery = function(){
				UTIL.object.merge( THIS_LINK, chainQueryFrame(CHAIN) );
			}
			fn.next.set.frameQuery = function(){
				UTIL.object.merge( NEXT_LINK, chainQueryFrame(CHAIN) );
			}
			//get functions
			//// //// //// ////
			fn.get = function(){return THIS_LINK;}
			fn.next.get = function(){return NEXT_LINK;}


			return fn;
		}

		return _interface;
})();