aRest.fn.core.structure.query.scope.In = (function(){
/////////////////////////////////////////

	var CONSTANT = aRest.fn.core.constant;
	var UTIL = aRest.fn.core.util;
	var DATA = aRest.fn.data;
	var STRUCTURE = aRest.fn.core.structure;

/////////////////////////////////////////
/////////////////////////////////////////

//this class will be provided a frame of reference in which to begin the query chain
//the frame can be a set of elements, or a global scope (by default) - the children in the frame will be examined in the calls of the chain
		var _interface = function(_chain, _execute){
			var In = function(query){

				return nextLink;
			}
			var _In = function(id, type){
				console.log(id,type);
			}
			//generate the next link in the chain
			var nextLink = UTIL.object.methodToCaps({
				and:In,
			});

			//apply narrowing options to query operator
			var options={};
			if(_chain.fn.functionOptions)
				options = _chain.fn.functionOptions(_In, nextLink);

			if(_chain.fn.recordAction)
				UTIL.object.merge(nextLink,_chain.fn.recordAction);

			//apply options to the function, and return
			In = UTIL.object.applyToFn(In,options);

			return In;
		}

		return _interface;
})();