aRest.fn.core.structure.query.create.New = (function(){
/////////////////////////////////////////

	var CONSTANT = aRest.fn.core.constant;
	var ERROR = aRest.fn.core.error;
	var UTIL = aRest.fn.core.util;
	var DATA = aRest.fn.data;
	var STRUCTURE = aRest.fn.core.structure;

/////////////////////////////////////////
/////////////////////////////////////////

/*Development cases:

the NEW interface will provide the user with a bare bones (uninserted entity)

*/
		var New = function(type, id, properties){
			var newRecordConstructor = STRUCTURE.data.Entry(type);

			//check property format
			if(!properties)properties = {};
			else if(!UTIL.type.isObject(properties))
				ERROR.query.throw.BadPropertyException("NEW",type, id, properties);

			//Determine the method that the user has used to insert a query
			var newResult = null;
			if(!type && id)newResult = New.from.query(id);
			else if(UTIL.type.isObject(id))newResult = New.from.object(newRecordConstructor,id,properties);
			else if(id)newResult = New.from.keyVal(newRecordConstructor,String(id),properties);
			else ERROR.query.throw.BadModifierException("NEW",type, id, properties);

			if(!newResult) return;
			else if(!UTIL.type.isArray(newResult))newResult = [newResult];
			//apply the new result to the chain's records
			this.result = this.result.concat(newResult);
		}

		New.from = {};
		New.from.object = function(constructor, ob, properties){
			var records = [];

			for(var i in ob){
				var newRecord = constructor(constructor, ob[i], properties);
				records.push(newRecord);
			}

			return records;
		}
		New.from.keyVal = function(constructor, key, properties){
			return constructor(constructor, key, properties);
		}
		New.from.query = function(query){
			//not implemented as of yet - requires query language invent
		}

		var _interface = function(chain){
			var self = STRUCTURE.query.ChainLink(chain);

			//apply scope modifiers to self
			self.set.modifier(New.bind(chain));

			//build next link
			/////////////////////////
			self.next.set.frameQuery();
			self.next.set.actions();
			self.next.set({
				
			})

			return self.get();
		}

		return _interface;
})();