aRest.fn.core.structure.query.util.Select = (function(){
/////////////////////////////////////////

	var CONSTANT = aRest.fn.core.constant;
	var UTIL = aRest.fn.core.util;
	var DATA = aRest.fn.data;
	var STRUCTURE = aRest.fn.core.structure;

/////////////////////////////////////////
	     var _interface = {};
/////////////////////////////////////////


//this class will be provided a frame of reference in which to begin the query chain
//the frame can be a set of elements, or a global scope (by default) - the children in the frame will be examined in the calls of the chain
		_interface.Class = function(_chain){
			var Select = function(query){

				return nextLink;
			}
			var _Select = function(id, type){
				_chain.frame.setTarget(id,type);
			}
			//generate the next link in the chain
			var nextLink = UTIL.object.methodToCaps({
				from:STRUCTURE.query.util.From.Class(_chain),
			});

			//apply narrowing options to query operator
			var options={};
			if(_chain.fn.functionOptions)
				options = _chain.fn.functionOptions(_Select, nextLink);


			//apply options to the function, and return
			Select = UTIL.object.applyToFn(Select,options);

			return Select;
		}

		return _interface;
})();