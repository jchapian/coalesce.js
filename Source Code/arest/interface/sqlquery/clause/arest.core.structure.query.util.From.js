aRest.fn.core.structure.query.util.From = (function(){
/////////////////////////////////////////

	var CONSTANT = aRest.fn.core.constant;
	var UTIL = aRest.fn.core.util;
	var DATA = aRest.fn.data;
	var STRUCTURE = aRest.fn.core.structure;

/////////////////////////////////////////
	     var _interface = {};
/////////////////////////////////////////

//this class will be provided a frame of reference in which to begin the query chain
//the frame can be a set of elements, or a global scope (by default) - the children in the frame will be examined in the calls of the chain
		_interface.Class = function(_chain){
			var From = function(query){

				return nextLink;
			}
			
			//generate the next link in the chain
			var nextLink = UTIL.object.methodToCaps({
				in:STRUCTURE.query.util.In.Class(_chain),
			});

			//apply narrowing options to query operator
			var options={};
			if(_chain.fn.functionOptions)
				options = _chain.fn.functionOptions(function(id,type){
					
				}, nextLink);

			if(_chain.fn.recordAction)
				options= UTIL.object.merge(options,_chain.fn.recordAction);

			//apply options to the function, and return
			options = UTIL.object.merge(options, nextLink);
			From = UTIL.object.applyToFn(From,options);

			return From;
		}

		return _interface;
})();