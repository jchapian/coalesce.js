aRest.fn.data.Vault = (function(){
/////////////////////////////////////////

	var CONSTANT = aRest.fn.core.constant;
	var UTIL = aRest.fn.core.util;
	var DATA = aRest.fn.data;
	var STRUCTURE = aRest.fn.core.structure;
	var ERROR = aRest.fn.core.error;

/////////////////////////////////////////
	     var _interface = {};

/////////////////////////////////////////
var getConstructor = function(prefix){return STRUCTURE.data.Entry(prefix);}


//will return entry point, given type constant
_interface.Class = function(){
	var _interface = {};
	var data = {};
		data[CONSTANT.parse.prefix.component] = {};
		data[CONSTANT.parse.prefix.group] = {};
		data[CONSTANT.parse.prefix.container] = {};
		data[CONSTANT.parse.prefix.module] = {};
		data[CONSTANT.parse.prefix.template] = {};

		//filter not implemented
		//action function [entity, container, name, index in name list]
		//efficiency is increased by explicitly using type and name to navigate the data structures
		var vaultRequest = function(type, name, actionFunction, filter){
			//we are looking at master records globally
			var searchIn = [];
			var rArray = [];
			//we are not looking for a specific type
			if(!type || type == CONSTANT.parse.operand.all)
				for(var i in data)//type
				searchIn.push(data[i]);
			else //we are looking for a specific type
				searchIn.push(data[type])

			//check containers for results
			for(var i in searchIn)
			{
				//not searching for a specific name, add all
				if(!name || name=='' || name == CONSTANT.parse.operand.all)
				{
					for(var j in searchIn[i])
					for(var k in searchIn[i][j])
					{
						if(!searchIn[i][j])break;

						if(filter && !filter.run(searchIn[i][j][k]))continue;
						rArray.push(searchIn[i][j][k]);
						if(actionFunction) actionFunction(searchIn[i][j][k], searchIn, i, j, k);
					}
				}
				//Searching for specific name, add only if exists
				else if(searchIn[i][name]) {
					for(var k in searchIn[i][name]){
						if(filter && !filter.run(searchIn[i][name][k]))continue;
						//record results
						rArray.push(searchIn[i][name][k]);
						//use evaluation
						if(actionFunction)
						actionFunction(searchIn[i][name][k], searchIn[i], name, k);
					}
				}
			}

			return rArray;
		}

	_interface.get = function(type, name, filter){
		return vaultRequest(type,name,null,STRUCTURE.data.base.DataRecord.Evaluator(filter));
	}	

	_interface.exec = function(type,name,filter,action){
		return vaultRequest(type,name,action,STRUCTURE.data.base.DataRecord.Evaluator(filter));
	}

	_interface.insert = function(type, name, parentEntity){
			//validate the submission
			if(ERROR.debug && !ERROR.data.validateNewEntity(type, name, parentEntity))return [];//failed check

			//the container for object to be placed, and new object constructed locally
			var container = data[type];
			var constructor = getConstructor(type);
			var newData = new constructor(name,parentEntity);

			//place the object in quick access storage
			if(!container[name]) container[name] = [newData];
			else container[name].push(newData);

			//apply the proper scope to the new object
			if(parentEntity && parentEntity.frame)
			parentEntity.frame.add(name,newData);
			//return the new data
			return [newData];	
	}
	_interface.remove = function(type, name, recordReference, filter){

			//make the removal action
			var removalFn = function(record, referenceObject, i){
					if(!recordReference)referenceObject[i] = [];
					else{
						for(var j in referenceObject[i]){
							if(referenceObject[i][j] == recordReference) {
								referenceObject[i].splice(j,1);
							}
						}
					}
				}

			//make request
			vaultRequest(type,name,removalFn,STRUCTURE.data.base.DataRecord.Evaluator(filter));console.log(data)
	}
	return _interface;
}

	return _interface;
})();