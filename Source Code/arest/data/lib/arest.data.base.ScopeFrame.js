aRest.fn.core.structure.scope.Frame = (function(){
/////////////////////////////////////////

	var CONSTANT = aRest.fn.core.constant;
	var UTIL = aRest.fn.core.util;
	var DATA = aRest.fn.data;
	var STRUCTURE = aRest.fn.core.structure;

/////////////////////////////////////////
	     var _interface = {};
/////////////////////////////////////////
_interface.Class = function(){
	var _children = {};
	var _interface = {};

	_interface.add = function(name, record){
		//apply new record
		if(_children[name])_children[name].push(record);
		else _children[name] = [record];
	}

	_interface.remove = function(filter){ 
		var children = STRUCTURE.scope.Frame.traverse(_children, false,STRUCTURE.data.base.DataRecord.Evaluator(filter), function(rOb, i, j){
			rOb[i].splice(j,1);
		});
		return children;
	}

	//get all
	
	_interface.get = function(){return STRUCTURE.scope.Frame.traverse(_children, false,STRUCTURE.data.base.DataRecord.Evaluator.apply(this,arguments));}
	_interface.get.all = function(){ return STRUCTURE.scope.Frame.traverse(_children, true,STRUCTURE.data.base.DataRecord.Evaluator.apply(this,arguments));}

	//execute a command on all
	_interface.exec = function(filter, action){return STRUCTURE.scope.Frame.traverse(_children, false,STRUCTURE.data.base.DataRecord.Evaluator(filter),action);}
	_interface.exec.all = function(filter,action){return STRUCTURE.scope.Frame.traverse(_children, true,STRUCTURE.data.base.DataRecord.Evaluator(filter),action);}

	return _interface;
}

//use this fn to traverse the frames contained by an array of records
//expected is the children of a scope object, and a list of filters to determine if they should be operated on
//returned is a list of all those that passed the filter test
_interface.traverse = function(_children, all, filter, userAction_FN){
		var rArray = [];
		//get children based on evaluation function
		for(var i in _children)
		for(var j in _children[i]){
			//pose correct
			if(filter.run(_children[i][j])){
				//record
				rArray.push(_children[i][j]);
				//perform action
				if(UTIL.type.isFunction(userAction_FN))
				userAction_FN(_children,i,j);
			}
			//attempt to provide recursion
			rArray = (all && _children[i][j].frame) ? rArray.concat(_children[i][j].frame.get.all(filter)) : rArray;
		}
		
	return rArray;
}

	return _interface;
})();