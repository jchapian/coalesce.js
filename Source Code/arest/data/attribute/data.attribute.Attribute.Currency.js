aRest.fn.core.structure.data.base.Attribute = (function(){
/////////////////////////////////////////

	var CONSTANT = aRest.fn.core.constant;
	var UTIL = aRest.fn.core.util;
	var DATA = aRest.fn.data;
	var STRUCTURE = aRest.fn.core.structure;
	
/////////////////////////////////////////
	     var _interface = {};
/////////////////////////////////////////

	_interface.Class = function(key,value,frame,callbacks){
		var _key = key,
		_value = value;
		
		//define outside interface
		_interface = {};

		_interface.get = function(){return _value; }
		_interface.set = function(value){
			//check if callback associated with setting of this value
			_value = value;
			return _value;
		}

		return _interface;
	}

	return _interface;
})();