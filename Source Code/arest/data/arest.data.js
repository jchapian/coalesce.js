//all methods provided by this module are tailored toward maintaining consistancy of internal data. That is all. It is up to external modules to make the appropriate calls to ensure consistancy.
//deletion calls will remove internal references to object, but it is up to external objects to remove their references to the objects, as well. The GC will collect when this is done.
aRest.fn.data = (function(){

	/////////////////////
	//IMPORT
	//////////////
	var CONSTANT = aRest.fn.core.constant;
	var UTIL = aRest.fn.core.util;
	var STRUCTURE = aRest.fn.core.structure;
	var ERROR = aRest.fn.core.error;


	var Vault = new STRUCTURE.data.Vault.Class();


	//all data is to be handled in a consistant manner, to maintain internal integrity
	//scope is assumed to be a reference to an entity in data (will be json)
	var dataAPI = {
		
		//scope is assumed to be record node
		add : function(type, name, parentEntity){
			return Vault.insert(type,name,parentEntity);
		},

		/*this will recursively destroy all children contained by the scope */
		//scope deletion is to be handled by individual records.
		//this method is not to be called by anything but the delete function provided by data records
		remove : function(type, name, recordReference){
			return Vault.remove(type, name, recordReference);
		},

		get : function(type, name, filter, scope){
			if(!type)type = CONSTANT.parse.operand.all;
			if(!name)name = CONSTANT.parse.operand.all;

			//determine best interface to return a result
			if(scope)
				scope.get.all()
			else
			return Vault.get(new STRUCTURE.data.base.DataRecord.Evaluator(filter, type, name));
		},

		Entry : function(type){
			switch(type){
				case CONSTANT.parse.prefix.component:
					return STRUCTURE.data.entity.Component;
					break;
				case CONSTANT.parse.prefix.group:
					return STRUCTURE.data.entity.Group;
					break;
				case CONSTANT.parse.prefix.container:
					return STRUCTURE.data.entity.Container;
					break;
				case CONSTANT.parse.prefix.template:
					return STRUCTURE.data.entity.Template;
					break;
				case CONSTANT.parse.prefix.module:
					return STRUCTURE.data.entity.Module;
					break;
			}
		}

	}
	/////////////////////
	//XXXXXXXXXXX

	//hide vault, and check error in front of it
	return dataAPI;
}).bind(aRest.fn.data)();