aRest.fn.core.structure.data.base.DataRecord = (function(){
/////////////////////////////////////////

	var CONSTANT = aRest.fn.core.constant;
	var UTIL = aRest.fn.core.util;
	var DATA = aRest.fn.data;
	var STRUCTURE = aRest.fn.core.structure;

/////////////////////////////////////////
/////////////////////////////////////////
//Class Definition




var _interface = function(type, id, properties, parent, callbacks){ 

	var _self = this;
	var _parent = (parent) ? parent : null;

	var _id = id,
		_attributes = {},
		_type = type,
		_frame= new STRUCTURE.scope.Frame.Class(),
		callbacks;

	//////INIT////// 



	///////PUBLIC INTERFACE///////
	_self.get = {};
	_self.set = {};

	//scope manipulations
	_self.frame = _frame;

	//attribute manipulations
	_self.get.attribute = function(name){
		if(!name){
			var rOb = {};
			for(var i in _attributes)
				rOb[i] = _attributes[i].get();
			return rOb;
		}
		else if(_attributes[name])
			return _attributes[name].get();
		else 
			return null;
	}

	_self.set.attribute = function(nameOrObject, value){
		if(typeof(nameOrObject) == 'string' && value){var tmp={}; tmp[nameOrObject]=value; nameOrObject = tmp;}
		if(!UTIL.type.isObject(nameOrObject)){
			//throw error
			return;
		}

		//loop the object ob attributes
		for(var name in nameOrObject){
			if(_attributes[name]) _attributes[name].set(nameOrObject[name]);
			else _attributes[name] = new STRUCTURE.data.base.Attribute.Class(name, nameOrObject[name]);
		}
	}
	//type manipulation
	_self.get.type = function(){return _type;}
	_self.set.type = function(t){_type = t;}

	//id manipulation
	_self.get.id = function(){return _id;}
	_self.set.id = function(id){_id = id;}

	//destructor
	_self.delete = function(){
		var dArray = [];
		//remove from current frame
		if(_parent != null)_parent.frame.remove(_self)

		//remove the data records contained in the scope of this object
		var containedNodes = _frame.get();
		for(var i in containedNodes){
			dArray = dArray.concat(containedNodes[i].delete());
		}

		//remove self from internal storage
		aRest.fn.data.remove(_type, _id, _self);

		//return chain
		return [_self].concat(dArray);

	}

	//init properties
	_self.set.attribute(properties);

	return _self;
}

//public access definitions


//handles multiple cases, and returns a function that will evaluate the conditions
	//traverse recursive or single layer
	//provide no query (fetch all)
	//query = string (fetch id)
	//query = function (evaluate function)
	//query = entity record (compare pointer locations)
	//query = type (return specified types)


	//input is an array, or comma seperated list of filters specified above, return is an array of condition checking functions
_interface.Evaluator = function(){
		var conditions = (function(){
					if(arguments.length == 0) return function(){return true;}
					//expand any provided arrays
					var tmp = [];
					for(var i in arguments){
						if(UTIL.type.isArray(arguments[i]))tmp = tmp.concat(arguments[i]);
						else tmp.push(arguments[i]);
						
					}

					//generate eval functions
					var rA = [];
					
					for(var i in tmp){
					var query = tmp[i];
					if(UTIL.type.isDataType(query)) //query is a data type
						rA.push(function(prospect){return (prospect.get.type() == query)});
		
					else if(UTIL.type.isFunction(query))//query is a function
						rA.push(function(prospect){return (query(prospect))});
		
					else if(UTIL.type.isDataRecord(query)) //query is an entity pointer
						rA.push(function(prospect){return (prospect == query)});
		
					else if(typeof(query) == 'string')//query is an id
						rA.push(function(prospect){return (prospect.get.id() == query)});
		
					else 
						rA.push(function(){return true;});
					}
					return rA;

		}).apply(this,arguments);

		var run = function(testCase){
			
			for(var i in conditions){
				if(!conditions[i](testCase))return false;
			}
			return true;
		}

		return {
			run :run,
		}
		
}

//traverse a set of DataRecord, and execute a command on each (provides single and multi layer traversal)
//filter uses the evaluator for the DataRecord class
_interface.exec = function(set, command, filter){
	return STRUCTURE.scope.Frame.traverse(set,false,new STRUCTURE.data.base.DataRecord.Evaluator(filter),command);
}
_interface.exec.all = function(set, command, filter){
	return STRUCTURE.scope.Frame.traverse(set,true,new STRUCTURE.data.base.DataRecord.Evaluator(filter),command);
}

	return _interface;
})();