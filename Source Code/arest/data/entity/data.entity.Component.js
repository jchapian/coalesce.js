aRest.fn.data.entity.Component = (function(){
/////////////////////////////////////////

	var CONSTANT = aRest.fn.core.constant;
	var UTIL = aRest.fn.core.util;
	var DATA = aRest.fn.data;
	var STRUCTURE = aRest.fn.core.structure;

/////////////////////////////////////////
/////////////////////////////////////////
	var _interface = function(id, properties, frame, callbacks){
		var record = new STRUCTURE.data.base.DataRecord(CONSTANT.parse.prefix.component, id, properties, frame, callbacks); //composition
			//component cannot have a frame
			record.frame = null;

		var Component = function(){
			
		}

		_interface = UTIL.object.clone(record,{});

		return record;

	}
	

	return _interface;
})();