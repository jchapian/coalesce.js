/* 
 ARest Application Operating Environment v0.0.0
 http://jordanchapian.com/labs/arest

--------------------------
 Jordan D Chapian
 The University of Tennessee, EECS Department
 PYA Analytics
--------------------------

 All Questions to be directed to: jordanchapian@gmail.com
 2/2/2014, initial change logged
 */

 var aRest = (function (window, undefined) {
	var aRest = function(selector, context)//consider selectors like group:group:component
	{	
		var test = ":)"
		return new aRest.fn.set(selector,context);
	}

	window.aR = aRest;
	//document associated with supplied window
	document = window.document;
	
	return aRest;
})(window);

//package contents
aRest.fn = aRest.prototype = {
	//module layers
	core: {},
	data:{},
	parse:{},
	interface:{},
	set : function(selector, context){
		this.groupSpecs = selector;
		return this;
	},
	init: function(){}
};

aRest.fn.set.prototype = aRest.fn;

