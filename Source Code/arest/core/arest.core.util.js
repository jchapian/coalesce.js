/////////////////////////////////////////////////////////
/////////////////////UTILITY LIBRARY/////////////////////
/////////////////////////////////////////////////////////

///////////INDEX///////////
/** @module util **/
aRest.fn.core.util = {
	type:{},
	dom:{},
	object:{}
};



///////////TYPE///////////
/** 
	@module util.type
	@main util
**/
aRest.fn.core.util.type = {
	/** Determine is the supplied argument is of type function 
		@public
		@function
	**/
	isFunction : function(functionToCheck) {
	 var getType = {};
	 return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
	},
	/** Determine is the supplied argument is of type object */
	isObject : function(obj) {
	  return obj === Object(obj) && Object.prototype.toString.call(obj) !== '[object Array]';
	},	
	/** Determine is the supplied argument is of type array */
	isArray: function( obj ) {
    return toString.call(obj) === "[object Array]";
	},
	/** Determine is the supplied argument is a recognized arest entity type */
	isDataType : function(prospectString){
		if(typeof(prospectString) != 'string')return false;
		var parseConstants = aRest.fn.core.constant.parse.prefix;
		for(var i in parseConstants){
			if(parseConstants[i] == prospectString)return true;
		}
		return false;
	},
	/** Determine if the supplied argument is a record instance */
	isDataRecord : function(prospect){
		return (prospect instanceof aRest.fn.core.structure.data.base.DataRecord.Class);
	}
},


///////////DOM///////////
aRest.fn.core.util.dom = {

	isDOMNode: function(v) {
	  if ( v===null ) return false;
	  if ( typeof v!=='object' ) return false;
	  if ( !('nodeName' in v) ) return false; 

	  var nn = v.nodeName;
	  try {
	    // DOM node property nodeName is readonly.
	    // Most browsers throws an error...
	    v.nodeName = 'is readonly?';
	  } catch (e) {
	    // ... indicating v is a DOM node ...
	    return true;
	  }
	  // ...but others silently ignore the attempt to set the nodeName.
	  if ( v.nodeName===nn ) return true;
	  // Property nodeName set (and reset) - v is not a DOM node.
	  v.nodeName = nn;

	  return false;
	}

},


///////////OBJECT///////////
aRest.fn.core.util.object = {

	isEmpty :function(value){
		return !(Boolean(value) && typeof value == 'object' && Object.keys(value).length > 0);
	},

	clone : function(from, to){
	    if (from == null || typeof from != "object") return from;
	    if (from.constructor != Object && from.constructor != Array) return from;
	    if (from.constructor == Date || from.constructor == RegExp || from.constructor == Function ||
	        from.constructor == String || from.constructor == Number || from.constructor == Boolean)
	        return new from.constructor(from);

	    to = to || new from.constructor();

	    for (var name in from)
	    {
	        to[name] = typeof to[name] == "undefined" ? aRest.fn.core.util.object.clone(from[name], null) : to[name];
	    }

	    return to;
	},

	merge : function() {//merge list of arguments into the first arg

	  var _mergeRecursive = function (dst, src) {
	    if ( aRest.fn.core.util.dom.isDOMNode(src) || typeof src!=='object' || src===null) {
	      return dst; 
	    }

	    for ( var p in src ) {
	      if( !src.hasOwnProperty(p) ) continue;
	      if ( src[p]===undefined ) continue;
	      if ( typeof src[p]!=='object' || src[p]===null) {
	        dst[p] = src[p];
	      } else if ( typeof dst[p]!=='object' || dst[p]===null ) {
	        dst[p] = _mergeRecursive(src[p].constructor===Array ? [] : {}, src[p]); 
	      } else {              
	        _mergeRecursive(dst[p], src[p]);
	      }
	    }
	    return dst;
	  }

	  // Loop through arguments and merge them into the first argument. 
	  var out = arguments[0];
	  if ( typeof out!=='object' || out===null) return out;
	  for ( var i=1, il=arguments.length; i<il; i++ ) {
	    _mergeRecursive(out, arguments[i]);
	  }
	  return out;
	},

	//send all of the properties associated with an object to uppercase
	methodToCaps :function(obj){
		var rOb = {};

		for(var i in obj){
			rOb[i] = rOb[String(i).toUpperCase()] = obj[i];
		}

		return rOb;
	},


	//apply an object's properties to a function
	applyToFn :function(fn, obj){
		var rFn = fn;
		for(var i in obj){
			rFn[i] = obj[i];
		}

		return rFn;
	}
}

///////////PROTOTYPAL///////////

//bind function scope
Function.prototype.bind = function(scope) {
  var _function = this;
  
  return function() {
    return _function.apply(scope, arguments);
  }
}

//inherit properties from object
Function.prototype.inheritsFrom = function( parentClassOrObject ){ 
	if ( parentClassOrObject.constructor == Function ) 
	{ 
		//Normal Inheritance 
		this.prototype = new parentClassOrObject;
		this.prototype.constructor = this;
		this.prototype.super = parentClassOrObject.prototype;
	} 
	else 
	{ 
		//Pure Virtual Inheritance 
		this.prototype = parentClassOrObject;
		this.prototype.constructor = this;
		this.prototype.super = parentClassOrObject;
	} 
	return this;
} 