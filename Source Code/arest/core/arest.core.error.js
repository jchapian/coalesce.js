/////////////////////////////////////////////////////////
//////////////////////ERROR LIBRARY//////////////////////
/////////////////////////////////////////////////////////
aRest.fn.core.error = {
	debug: true,
	
	data :{},
	structure:{},
	parse:{},
	interface:{},
}

//query
aRest.fn.core.error.query = {};
aRest.fn.core.error.query.throw = {};

aRest.fn.core.error.query.throw.BadModifierException = function(queryDesc, type, id, properties){
	console.error("Provided incorrect arguments to: "+queryDesc+"\n-- "+type+", "+id+", "+properties+" --");
}
aRest.fn.core.error.query.throw.BadPropertyException = function(queryDesc, type, id, properties){
	console.error(queryDesc+" expects properties to be of type [object], but provided was: ["+typeof(properties)+"]");
}