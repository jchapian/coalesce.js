/////////////////////////////////////////////////////////
////////////////////CONSTANT LIBRARY/////////////////////
/////////////////////////////////////////////////////////

aRest.fn.core.constant = {
	structure:{

	},
	parse:{

		prefix:{

			group:'+',
			container:'++',
			component:'++++',
			template:'+++++',
			module: '+++++++',
			

		},

		operand:{

			within:':',
			all:'*',

		}
	},
	interface:{

	},
	data:{
		containerName : 'aRest',
		id : '_aRest_id',
		type : '_aRest_type',
		scope : '_aRest_scope',
		children: '_aRest_children',
		attribute: '_aRest_attribute'
	},
}