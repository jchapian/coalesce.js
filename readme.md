  __   ____  ____  ____  ____      __  ____ 
 / _\ (  _ \(  __)/ ___)(_  _)   _(  )/ ___)

/    \ )   / ) _) \___ \  )(  _ / \) \\___ \

\_/\_/(__\_)(____)(____/ (__)(_)\____/(____/


Autonomously Restrictive JavaScript (ARest) is a JavaScript engine that provides a medium for software engineers to build applications on the web browser. The conceptual ideas are meant to abstract away from the document object model, and provide a new application model, specifically suited for building large scale maintainable applications by forcing code to be modular and autonomous. This new model removes the requirement to statically type full GUI applications, and removes the need for the programmer to maintain alternative HTML data for components of the application within the javascript source code. ARest provides a means to address complex dynamics within large-scale applications by use of a transmission network, in which abstractive layers of the application are able to receive generated signals, and respond functionally. Thus, allowing the programmer to handle a large number of interactions between data entities, and various states that the application must assume. The use of this engine will dramatically reduce code complexity of large-scale JavaScript applications, and provide standardized efficiency for DOM manipulations.

http://jordanchapian.com/labs/aRest/aRestAbout.html