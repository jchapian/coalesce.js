//////////////////////////////////////////////Theme//////////////////////////////////////////////
//---------------------------------------------------------------------------------------------//
//the theme can be accessed from the object, by object.theme - and it gives the developer full control over how elements are 
//formatted. This decision was made with reuse in mind. If modifications are needed to wrap a module, it can be done by making
//alterations to the prototypes of the following functions.
//default is to xhtml tags for common input elements, and html5 tags for moden complient browsers
//these functions have no knowledge of the internals of the module, but rather are passed information that can be read and formatted

var importTheme = {
	formPrefix:"_aRest_F_",
	divPrefix: "_aRest_C_",
	optionPrefix: "_aRest_O_",
	headerPrefix: "_aRest_H_",
	headerPrefixGroup: "_aRest_HG_",
	labelPrefix: "_aRest_L",

	formatheader_individual : function(object){
		return "<h3 id="+this.headerPrefix+object["id"]+">"+object["header"]+"</h3>\n";
	},
	formatheader_group : function(group){
		return "<h3 id="+this.headerPrefixGroup+group["id"]+">"+group["header"]+"</h3>\n";
	},

	formatCat : function(catData, options){
		var rVal = "<h1>"+catData["string"]+"</h1>"
		    rVal += "<div id='_aRest_C_"+catData['id']+"'>\n";
			rVal +=    "\t<form id='_aRest_F_"+catData['id']+"'>\n";
			rVal += options;
			rVal +=    "\t</form>\n";
			rVal += "</div>\n";
		return rVal;
	},

	//text field (xhtml)
	formatField : function(options, group){
		var rVal = "";
		
		if(!options.length){//single option
			if(options["string"] != null) rVal+= "<label for='"+this.optionPrefix+options["id"]+"'\n>"+options["string"]+"</label>";
			rVal+= "\t<input class='_aRestHidden _aRest_O' type='text' id='"+this.optionPrefix+options["id"]+"' />\n";
		}
		else//multiple options
		for(var i=0;i<options.length;i++){
			if(options["string"]) rVal+= "<label for='"+this.optionPrefix+options[i]["id"]+"'\n>"+options[i]["string"]+"</label>";
			rVal+= "\t<input class='_aRestHidden _aRest_O' type='text' id='"+this.optionPrefix+options[i]["id"]+"' />\n";
		}

		return rVal;
	},

	//check box group (or single) (xhtml)
	formatCheck : function(options, group){
		var rVal = "";
		
		if(!options.length){//single option
			if(options["string"]) rVal+= "<label for='"+this.optionPrefix+options["id"]+"'\n>"+options["string"]+"</label>";
			rVal+= "\t<input class='_aRestHidden _aRest_O' type='checkbox' id='"+this.optionPrefix+options["id"]+"' />\n";
		}
		else//multiple options
		for(var i=0;i<options.length;i++){
			if(options[i]["string"]) rVal+= "<label for='"+this.optionPrefix+options[i]["id"]+"'\n>"+options[i]["string"]+"</label>";
			rVal+= "\t<input class='_aRestHidden _aRest_O' type='checkbox' id='"+this.optionPrefix+options[i]["id"]+"' />\n";
		}
		
		return rVal;
	},

	//radio group (or single) (xhtml)
	formatRadio : function(options, group){
		var rVal = "";
		
		if(!options.length){//single option
			if(options["string"]) rVal+= "<label for='"+this.optionPrefix+options["id"]+"'\n>"+options["string"]+"</label>";
			rVal+= "\t<input class='_aRestHidden _aRest_O' type='checkbox' id='"+this.optionPrefix+options["id"]+"' />\n";
		}
		else//multiple options
		for(var i=0;i<options.length;i++){
			if(options[i]["string"]) rVal+= "<label for='"+this.optionPrefix+options[i]["id"]+"'\n>"+options[i]["string"]+"</label>";
			rVal+= "\t<input class='_aRestHidden _aRest_O' type='checkbox' id='"+this.optionPrefix+options[i]["id"]+"'  name='"+this.optionPrefix+group["id"]+"'/>\n";
		}
		
		return rVal;
	},


	//caution, interface to core calls, changing these details will affect core process
	//use about calls to change formatting, this is intended for those who have
	//experience with this code
	formatObject : function(options, group){	
		var rObject = "";
		var type;
		if(group != null)
		{
			type = group["type"];
			if(group["header"] != null)
				rObject += this.formatheader_group(group);
		}
		else {
			type = options["type"];
			if(options["header"] != null) rObject += this.formatheader_individual(options);
		}
		switch(type){
			case "field":
				rObject += this.formatField(options, group);
				break;
			case "check":
				rObject += this.formatCheck(options, group);
				break;
			case "radio":
				rObject += this.formatRadio(options, group);
				break;
		}

		return rObject;
	},

}
