var aRest_theme_wizard = {
	
	//theme control variables
	sideBarId : "aRestWiz_side",
	sideBarBuffer  : "",
	ContentsBuffer : "",

//theme application handles
	sendHTML : function(){
		var htmlBuffer = "";
		htmlBuffer += "<div class='span2'>";
		htmlBuffer += 	this.sideBarBuffer;
		htmlBuffer += "</div>";
		htmlBuffer += "<div class='span7'>";
		htmlBuffer += 	this.ContentsBuffer;
		htmlBuffer += "</div>";

		return htmlBuffer;
	},


	//you recieve a full formatted set of options and groups (based on your specifications) in html format - stored in options
	//you also recieve your catData object stored in your data file - the only html that is applied to the container that is associated with your theme
	//is that which is specified in the sendHTML function
	//this is the final level of abstraction
	formatCat : function(catData, options){

		//build contents of module
		var rVal  = "<h1 class='"+aRest_helper.catObject+" "+aRest_helper.inactiveObject+"' id='"+aRest_helper.catPrefix+catData['id']+"'>";
			rVal +=      catData["label"];
			rVal +=     "</h1>";
		    rVal += "<div class='"+aRest_helper.catObject+" "+aRest_helper.inactiveObject+"' id='"+aRest_helper.catPrefix+catData['id']+"'>\n";
			rVal += 	"<form id='_aRest_F_"+catData['id']+"'>";
			rVal += 		options;
			rVal += 	"</form>";
			rVal += "</div>";

			//add text to side bar
			this.sideBarBuffer += "<h6 class='"+aRest_helper.catObject+" "+aRest_helper.inactiveObject+"' id='"+aRest_helper.catPrefix+catData['id']+"'>";
			this.sideBarBuffer +=    catData["label"];
			this.sideBarBuffer += "</h6>";

			this.ContentsBuffer += rVal;
		//return rVal;
	},


	formatHeader : function(object, group){
		//determine naming convention
		var idPrefix = aRest_helper.optionPrefix;
		var classAssignment = aRest_helper.optionObject;
		var headerData = object;

		if(group != null){
			idPrefix = aRest_helper.groupPrefix;
			classAssignment = aRest_helper.groupObject;
			headerData = group;
		}

		//output data
		var rHeader ="";
		if((!object && !group) || (!headerData["header"]))return rHeader;
		 
		rHeader += "<h3 id="+idPrefix+headerData["id"]+" class='"+classAssignment+" "+aRest_helper.inactiveObject+"'>";
		rHeader += headerData["header"];
		rHeader += "</h3>\n";

		return rHeader;
	},

	//text field (xhtml)
	formatField : function(options, group){
		if(!options.length) options = new Array(options);
		var rVal = "";

		for(var i=0;i<options.length;i++){
			if(options[i]["label"] != null){
				rVal+= "<label class='"+aRest_helper.optionObject+" "+aRest_helper.inactiveObject+"' id='"+aRest_helper.optionPrefix+options[i]["id"]+"' for='"+aRest_helper.optionPrefix+options[i]["id"]+"'\n>";
				rVal+=      options[i]["label"];
				rVal+= "</label>";
			}
			rVal+= "\t<input class='"+aRest_helper.optionObject+" "+aRest_helper.inactiveObject+"' type='text' id='"+aRest_helper.optionPrefix+options[i]["id"]+"' />\n";
		}

		return rVal;
	},


	//check box group (or single) (xhtml)
	formatCheck : function(options, group){
		if(!options.length) options = new Array(options);
		var rVal = "";

		for(var i=0;i<options.length;i++){
			if(options[i]["label"] != null){
				rVal+= "<label class='"+aRest_helper.optionObject+" "+aRest_helper.inactiveObject+"' id='"+aRest_helper.optionPrefix+options[i]["id"]+"' for='"+aRest_helper.optionPrefix+options[i]["id"]+"'\n>";
				rVal+=      options[i]["label"];
				rVal+= "</label>";
			}
			rVal+= "\t<input class='"+aRest_helper.optionObject+" "+aRest_helper.inactiveObject+"' type='checkbox' id='"+aRest_helper.optionPrefix+options[i]["id"]+"' />\n";
		}
		
		return rVal;
	},


	//radio group (or single) (xhtml)
	formatRadio : function(options, group){
		if(!options.length) options = new Array(options);
		var rVal = "";

		for(var i=0;i<options.length;i++){
			if(options[i]["label"] != null){
				rVal+= "<label class='"+aRest_helper.optionObject+" "+aRest_helper.inactiveObject+"' id='"+aRest_helper.optionPrefix+options[i]["id"]+"' for='"+aRest_helper.optionPrefix+options[i]["id"]+"'\n>";
				rVal+=	 options[i]["label"];
				rVal+= "</label>";
			}
			//<input ..... /> split for conditional attributes
			rVal+= "\t<input class='"+aRest_helper.optionObject+" "+aRest_helper.inactiveObject+"' type='radio' id='"+aRest_helper.optionPrefix+options[i]["id"]+"' ";
			if(group != null)rVal+= "name='"+aRest_helper.optionPrefix+group["id"]+"' ";
			rVal+= "/>\n";
		}
		
		return rVal;
	},


	//caution, interface to core calls, changing these details will affect core process
	//use about calls to change formatting, this is intended for those who have
	//experience with this code
	formatData : function(options, group){	
		var rObject = "";
		var type = (group != null) ? group["type"] : options["type"];
		rObject += this.formatHeader(options,group);

		switch(type){
			case "field":
				rObject += this.formatField(options, group);
				break;
			case "check":
				rObject += this.formatCheck(options, group);
				break;
			case "radio":
				rObject += this.formatRadio(options, group);
				break;
		}

		return rObject;
	},



//event emission handles

	handle_set_inactive : function(domObject, recordEntry){
		if(!$(domObject).hasClass(aRest_helper.inactiveObject))
			$(domObject).addClass(aRest_helper.inactiveObject)

	},

	handle_set_active: function(domObject, recordEntry){
		if($(domObject).hasClass(aRest_helper.inactiveObject))
			$(domObject).removeClass(aRest_helper.inactiveObject)
	}

}