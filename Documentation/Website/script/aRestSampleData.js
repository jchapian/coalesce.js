
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////Sample Inputs for function//////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////Data Format//////////////////////////////////////////
//---------------------------------------------------------------------------------------------//
//the idea behind this data format is to provide a interface that is dynamic based on user selection
//thus the inclusion of restraints. Json is used, so that alternative data can be stored about an option without affecting functionality
//This also provides a fast retrieval method for all data that the user has selected without using browser post or get methods
//categories will divide the wizard into steps - this is meant to wrap additional code for wizard interface
//there are similarities to third normal database form, so this may be extended for database use
//see documentation for relational algebra samples of common operations, and representation using set theory (If interested)
//I assumed small data sets, so many functions use linear lookup time to make alterations to data. This allows json to be formatted in many ways
//see documentation and demonstrations


var aRest_sampleData = {
	container : [
		{"id":0, "elementId":"wizard", "theme": aRest_theme_wizard}
		//{id="custom1", "elementId:customMenu:'test'}
		],


	cats : [ //must allow code to recognize order attribute
		{"id":0, "container":0, "label":"Best Sports"},
		{"id":"why", "container":0, "label":"Reason"},		//0
		{"id":1, "container":0, "label":"Best Player"}
		
	],


	options : [	//define a group to create a grouped element (drop down list, radio, etc. type will take on that of group)
		///////////page 1//////////
		//group-0
		{"id":1, "FK_cat":0, "label":"Football",   "group":0},	//0
		{"id":2, "FK_cat":0, "label":"Soccer",   "group":0, "order":0},
		{"id":3, "FK_cat":0, "label":"Rugby",   "group":0},
		
		//cat- why
		{"id":"why", "FK_cat":"why", "label":"why?",   "type":"field", "header":"Why do you like this sport?", "order":2},

		//page 2
		{"id":4, "FK_cat":1, "label":"Walter Payton",  "group":1},
		{"id":5, "FK_cat":1, "label":"Lawrence Taylor",  "group":1},
		{"id":6, "FK_cat":1, "label":"Joe Montana",  "group":1},
		{"id":7, "FK_cat":1, "label":"Jerry Rice",  "group":1},

		{"id":8, "FK_cat":1, "label":"Lionel Messi",  "group":1},
		{"id":9, "FK_cat":1, "label":"Cristiano Ronaldo",  "group":1},
		{"id":10, "FK_cat":1, "label":"Pele",   "group":1},
		{"id":11, "FK_cat":1, "label":"Diego Maradona",   "group":1},

		{"id":12, "FK_cat":1, "label":"Shane Williams",   "group":1},
		{"id":13, "FK_cat":1, "label":"Naas Botha",  "group":1},
		{"id":14, "FK_cat":1, "label":"Percy Montgomery", "group":1},
		{"id":15, "FK_cat":1, "label":"Brian O’Driscoll", "group":1}
	],

	group : [
		{"id":0, "name":"Sports", "type":"radio", "header":"So, what's the best sport?", "order":1, "sort":"asc"},
		{"id":1, "name":"Players", "type":"check", "header": "Nice, so do you know any of these players?", "sort":"asc"}
	],

	//active options impose the availability of additional options
	//the condition is imposed on the visibility of a field when it is requested.
	restraints : [
		{"condition":["1|2|3"], "option":["why"]}, //allow or statement
		{"condition":[1, "why"], "option":[4,5,6,7]},
		{"condition":[2, "why"], "option":[8,9,10,11]},
		{"condition":[3, "why"], "option":[12,13,14,15]}
	]
};