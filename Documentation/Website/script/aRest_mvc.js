//Jordan D Chapian
//aRest 0.0.0
//Description:
//aRest is a json driven restraint based form wizard that was built to wrap existing wizard interfaces
//to provide a more dynamic experience, also to provide a means of associative data storage or decision making in json format
//decisions can be made on the client based on encoded json information without an additional request to the server.
//visible input options are based on a set of restraints which are encoded as an array of foreign keys

var aRest = (function(user_data){
	//developer has control over theme and data:
	var dataSet;
	if(user_data != null) dataSet = user_data;
	//but no control over the core: (unless core source altered - please provide sub version if done)
	var modal = new aRest_modal(dataSet);
	var view = new aRest_view(modal);
	var controller = new aRest_controller(modal,view);
});

////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////       Core Function      //////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////Helper Methods//////////////////////////////////////////
//---------------------------------------------------------------------------------------------//
//contains misc helper functions - fully public for use within object - that the core uses often
var aRest_helper = {


	//class specifiers
	catObject:      "_aRest_C",
	optionObject:   "_aRest_O",
	groupObject:    "_aRest_G",
	inactiveObject:   "_aRestHidden",
	//id prefix specifiers
	formPrefix:     "_aRest_F_",
	catPrefix:      "_aRest_C_",
	optionPrefix:   "_aRest_O_",
	groupPrefix:    "_aRest_G_",
	//mode flags
	ascendingSort:	"asc",
	decendingSort:	"desc",

	dataFormatValidate : function(data){

	},


	sortOptions: function(list, group){
		var sortMethod;
		var rArray = list.slice();

		if(group && group["sort"]) sortMethod = group["sort"];
		//provide initial sort
		rArray = this.sortOnFlags(rArray,sortMethod);
		//provide user defined order now
		rArray = this.sortOnOrder(rArray);

		return rArray;
	},

	//takes a collection and an int order, and returns the position of the first record with int order
	nextRecordByOrder : function(collection, order){
		if(!collection) return null;

		for(var i=0; i< collection.length;i++){
			if(collection[i]["order"] == order)return i;
		}

		return null;
	},


	sortOnOrder: function(options){
		var rArray = options.splice(0);
		for(var i=0;i<rArray.length;i++)
		{
			if( rArray[i]["order"] != null && rArray[i]["order"] != i){
				var tmp = rArray[i];
				rArray.splice(i,1);
				rArray.splice(tmp["order"], 0, tmp);
				
			}
		}

		return rArray;
	},

	//takes dom object that has been flagged for an event by aRest, and emits the event to the theme
	//object is the object active in the DOM
	//masterEntry is the data entry associated with the object (must fetch from modal)
	//themeEntry is the entry point to the them associated with the object that will handle state event
	//returns -1 on failure
	emitStateEvent : function(object, masterEntry, themeEntry, type){
		if(themeEntry == null)return false; //fail
		//determine the type of handle that must be used

		if(type == "active")//try to outsource active state handles
		{
			if(themeEntry.handle_active == null) return false; //fail

			themeEntry.handle_set_active(object, masterEntry);
			return true;//success
		}
		else if(type == "inactive"){//try to outsource inactive state handles
			if(themeEntry.handle_set_inactive == null) return false; //fail

			themeEntry.handle_set_inactive(object, masterEntry);
			return true; //success
		}

		return false;//fail
	},

	sortOnFlags: function(list, flag){
		if(!list)return null;
		var rArray = list.slice(0);

		if(flag == this.decendingSort || flag == this.ascendingSort){
			rArray = rArray.sort(function(a,b){
				a = (!a || !a["label"]) ? null : a["label"].toLowerCase();
				b = (!b || !b["label"]) ? null : b["label"].toLowerCase();
				if(!a) return 1;
				if(!b) return -1;
				return (a>b) ? 1: -1;
			});
		}
		if(flag == this.decendingSort) rArray.reverse();

		return rArray;
	},


	optionIdToJsonId : function(nodeid){
		var prefix = "_aRest_O_";	// may make this an alter field in theme file (hence formatting)
		return nodeid.substr(prefix.length, nodeid.length - 1);
	},


	extractOrStatement : function(statement){
		if( (typeof statement != 'string') || (statement.indexOf("|") == -1) )return [statement];
		return statement.split("|");
	},


	formElementActive : function(prospectNode) {
		//prospect node must be determined to be active or inactive
		var elementType = $(prospectNode).prop('tagName');
		//if working with input field, reduce to type property for switch
		if($(prospectNode).prop('tagName') == "INPUT") elementType = $(prospectNode).prop('type').toUpperCase();
		//determination based on element type
		switch(elementType){
			case "CHECKBOX":
				return (prospectNode.checked);
				break;
			case "TEXT":
				return (prospectNode.value != "");
				break;
			case "RADIO":
				return (prospectNode.checked);
				break;
		}
	}

}
/////////////////////////////////////////////Modal///////////////////////////////////////////////
//---------------------------------------------------------------------------------------------//
//Maintain data integrity, and provide interface into data format, so that methods outside of 
//the module don't have to worry about the formatting 
//with that said, this module will provide an interface for extracting information from subsets of data provided by itself
//so as to minimize the other modules dependency on the data formatting. This will increase maintainability

function aRest_modal(dataSet){
	//get data from developer (json format documented)
	var cats = dataSet.cats;
	var options = dataSet.options;
	var group = dataSet.group;
	var restraints = dataSet.restraints;
	var container = dataSet.container;

	//this function will determine the visibility of options based on a set of restraints user provided
	//if unrestrained, it will be visible
	this.refactorOptions = function(){
		//set all visible
		this.setAllVisible();

		//determine what options need to be displayed, converting lone conditions to arrays if needed
		for(var i=0; i<restraints.length;i++){
			//deal with the special case of restraints and options being non array - early on, so assumptions may be made
			var conditions = typeof restraints[i]["condition"] == 'string' ? [restraints[i]["condition"]] : restraints[i]["condition"];
			var restrainedOptions = typeof restraints[i]["option"] == 'string' ? [restraints[i]["option"]] : restraints[i]["option"];
			var met = true; //assume

			//loop all conditions, and see if one is false
			for(var j=0; j<conditions.length;j++){

				//consider an or statement, and check if one is true
				var possibleSatisfiers = aRest_helper.extractOrStatement(conditions[j]);

				for(var q=0; q<possibleSatisfiers.length;q++){
					var check = this.getOptionById(possibleSatisfiers[q]); //option to check for status
					if( (check["status"] == 1) )break; //true
					else if( (q == possibleSatisfiers.length -1) && (check["status"] == 0) ){ //false
						met = false; //retract assumption
						break;
					}
				}
			}
			//change display property of field based on the results
			for(var k=0;k<restrainedOptions.length;k++){
				this.getOptionById(restrainedOptions[k])["display"] = (met==true);
			}		
		}

		//determine what cats need to be displayed
		for(var i=0; i<cats.length; i++){
			if(this.catActive(cats[i])) cats[i]["display"] = true;
			else cats[i]["display"] = false;
		}

		//determine what groups need to be displayed
		for(var i=0; i<group.length; i++){
			if(this.groupActive(group[i])) group[i]["display"] = true;
			else group[i]["display"] = false;
		}
	}


	//returns record provided a dom element
	this.recordOf = function(DomElement){
		var testCases = new Array(aRest_helper.optionPrefix, aRest_helper.catPrefix, aRest_helper.groupPrefix); //extend to include groups
		var DOMid = DomElement.id;

		if(DOMid == null)return null;

		for(var i=0;i<testCases.length;i++){
			//get an id core can understand
			if(DOMid.indexOf(testCases[i]) != -1){
				var coreId = DOMid.substr(testCases[i].length, DOMid.length - 1);
				//determine what to return
				switch(testCases[i]){
					case aRest_helper.optionPrefix:
						return this.getOptionById(coreId);
						break;
					case aRest_helper.catPrefix:
						return this.getCats(coreId);
						break;
					case aRest_helper.groupPrefix:
						return this.getGroupById(coreId);
						break;
				}
			}
		}

		return null;
	}


	//will return if a cat has active options or likewise
	this.catActive = function (cat){
		for(var i=0;i<options.length;i++){
			//if we must display content on this page, it is active
			if((options[i]["FK_cat"] == cat["id"]) && (options[i]["display"] == true))
				return true;
		}

		return false;
	}


	//will determine if any options in a group are active or likewise
	this.groupActive = function (group){

		for(var i=0;i<options.length;i++){
			if(options[i]["group"] == null) continue;

			if((options[i]["group"] == group["id"]) && (options[i]["display"] == true))
				return true;
		}

		return false;
	}


	//will return group information about a given option set, returned in same type as defined
	this.extractGroupInformation = function(optionSubset){
		if(optionSubset == null)return group;
		var tmp = new Array();
		//build list of groups that are in the set
		for(var i=0;i<optionSubset.length;i++){
			if( (optionSubset[i]["group"] != null))
			{
				if(tmp.indexOf(optionSubset[i]["group"]) == -1) tmp.push(optionSubset[i]["group"]);
			}
		}

		//gather needed groups if they exist
		var rArray = new Array();
		for(var j=0; j<tmp.length;j++){
			var tmpGroup = this.getGroupById(tmp[j]);
			if(tmpGroup)rArray.push(tmpGroup);
		}
		return rArray;
	}


	//accepts cat, or option record - determined by required attributes of the two, and returns a theme associate with them
	this.getThemeEntry = function(data){
		if(data["container"] != null)//its a category
		{
			var container = this.getContainers(data["container"]);
			return (container["theme"] == null) ? null : container["theme"];
		}
		else if(data["FK_cat"] != null)//its an option
		{
			var category = this.getCats(data["FK_cat"]);
			var container = this.getContainers(category["container"]);
			return (container["theme"] == null) ? null : container["theme"];
		}
	}
	

	//extractGroupNodes: returns array of objects associated with the groupId in a subset of the option data object
	this.extractGroupNodes = function(groupId, optionSubset){
		var rArray = new Array();
		for(var i=0;i<optionSubset.length;i++){
				if(optionSubset[i]["group"] == groupId) rArray.push(optionSubset[i]);
		}
		return rArray;
	}


	//change to simple update the state based on 0 or 1
	//find the option based on id (not assuming id is numeric and thus no constant lookup)
	this.changeStatus = function(uId, status){
		var update = this.getOptionById(uId);
		if(update != null) update["status"] = status;
	}


	//getcats() -- Will return a specific cat record provided id, all cats provided nothing, and all cats in a container, provided container
	this.getCats = function(id, containerRecord){
		if(!id && !containerRecord)return cats; //return all cats
		
		if(!containerRecord && id){ //return cat with specific id
			for(var i=0; i< cats.length; i++){
				if(cats[i]["id"] == id)return cats[i];
			}
		}
		else if(containerRecord && !id){ //return all cats for container
			var rArray = [];
			for(var i=0;i<cats.length;i++){
				if(cats[i]["container"] == containerRecord["id"])rArray.push(cats[i]);
			}
		}
		return null;
	}


	this.getContainers = function(containerId){
		if(containerId == null)return container;

		for(var i=0;i<container.length;i++){
			if(containerId == container[i]["id"])return container[i];
		}

		return null;
	}


	//getcatoptions() return all options - getcatoptions(id) will return options specific to catid
	this.getCatOptions = function(catId){
		if(catId == null)return options;
		else{
			var rArray = new Array();

			for(var i=0;i<options.length;i++){
				if(options[i]["FK_cat"] == catId)rArray.push(options[i]);
			}

			return rArray;
		}
		return null;
	}


	this.setAllVisible = function(){
		//options
		for(var i=0;i<options.length;i++){
			options[i]["display"] = 1;
		}
		//cats
		for(var i=0;i<cats.length;i++){
			cats[i]["display"] = 1;
		}
	}


	this.getOptionById = function(id){
		for(var i=0;i<options.length;i++){
			if(options[i]["id"] == id)return options[i];
		}
	}


	this.getGroupById = function(id){
		for(var i=0;i<group.length;i++){
			if(group[i]["id"] == id)return group[i];
		}
	}

}

/////////////////////////////////////////////View////////////////////////////////////////////////
//---------------------------------------------------------------------------------------------//
//will determine formatting of the data - providing:
//option initilization utilizing theme file
//view state format (visible, invisible, disabled, enabled... etc)
//return information about the view to other modules
function aRest_view(modal){


	var viewInit = function(){ //refactor prospect
		var cats = modal.getCats();
		var containers = modal.getContainers();
		
		//get components associated with each container
		for(var i=0;i<containers.length;i++){
			//get the theme associated with the container (the module), and the location for html to be exported
			var theme = containers[i]["theme"];
			var dumpLocation = document.getElementById(containers[i]["elementId"]);

			//get the categories that are associated with the container
			var associatedCats = modal.getCats(null, containers[i]["id"]);
			var containerHTML = "";

			//apply initial html

			//build a list of options associated with the categories
			for(var j=0; j < associatedCats.length; j++){
				var catOptions = modal.getCatOptions(associatedCats[j]["id"]);
				var catGroups = modal.extractGroupInformation(catOptions);
				//put into groups of (ungrouped and grouped)
				//ungrouped
				var ungroupedOptions = modal.extractGroupNodes(null, catOptions);
				//sort ungrouped
				ungroupedOptions = aRest_helper.sortOptions(ungroupedOptions, null);

				//grouped
				
				var groupedOptions = new Array();
				
				for(var k=0;k<catGroups.length;k++){
					groupedOptions[k] = new Array();
					groupedOptions[k] = modal.extractGroupNodes(catGroups[k]["id"], catOptions);
					//sort group options
					groupedOptions[k] = aRest_helper.sortOptions(groupedOptions[k], catGroups[k]);
				}

				//build the html for the category
				constructCat(associatedCats[j], catGroups.slice(), groupedOptions, ungroupedOptions, theme);
			}
			
			if(dumpLocation != null) dumpLocation.innerHTML = theme.sendHTML();
		}
	}


	//builds interface based on theme information
	var constructCat = function(catData, groupData, groupNodes, loneNodes, theme){	
		var rVal = "";
		var groupReference = groupData.slice();
		var loneReference = loneNodes.slice();
		var i=0;
		//must maintain order while adding components to a module
		while((groupReference.length > 0 || loneReference.length > 0))
		{
			var gTmp = aRest_helper.nextRecordByOrder(groupReference, i);
			var lTmp = aRest_helper.nextRecordByOrder(loneReference, i);
			var matchFound = true;

			//if there is no option with this record, add only one
			if(gTmp == null && lTmp == null){
				gTmp = aRest_helper.nextRecordByOrder(groupReference, null);
				lTmp = aRest_helper.nextRecordByOrder(loneReference, null);
				matchFound = false;
			}

			//get formatting for the records
			if(gTmp != null) {
					rVal += theme.formatData(groupNodes[gTmp], groupReference[gTmp]);
					groupReference.splice(gTmp, 1);
			}
			if( (matchFound || gTmp == null) && (lTmp != null))//Only add one unordered, but all ordered in this position
			{
				rVal += theme.formatData(loneReference[lTmp]);
				loneReference.splice(lTmp, 1);
			}

			i++;
		}

		rVal = theme.formatCat(catData, rVal);
	}


	this.updateVisibility = function(){
		//possible updates: options, cats, or groups
		var searchClasses = "."+aRest_helper.optionObject+", "+"."+aRest_helper.catObject+", "+"."+aRest_helper.groupObject;
		var testObjects = $(searchClasses);

		//update options and associated labels
		$(testObjects).each(function(){
			//get a reference to the object's records in the data modal
			var objectRecord = modal.recordOf(this);
			var display = objectRecord["display"];
			//get a reference to the theme file associated with the module
			var theme = modal.getThemeEntry(objectRecord);
			var eventHandled;
			//if it is hidden, and it should not be, unhide it
			if(display && $(this).hasClass(aRest_helper.inactiveObject)){
				if(theme != null) //see if the theme will handle the event
					eventHandled = aRest_helper.emitStateEvent(this, objectRecord, theme, "active");
				if(!eventHandled) //the theme cannot handle the event
					$(this).removeClass(aRest_helper.inactiveObject);
			}//unhidden and shouldn't be
			else if(!display && !$(this).hasClass(aRest_helper.inactiveObject)){
				if(theme != null) //see if the theme will handle the event
					eventHandled = aRest_helper.emitStateEvent(this, objectRecord, theme, "inactive");
				if(!eventHandled) //the theme cannot handle the event
					$(this).addClass(aRest_helper.inactiveObject);
			}
		});
	}

	//init
	viewInit();
}
///////////////////////////////////////////Controller////////////////////////////////////////////
//---------------------------------------------------------------------------------------------//
//will set events on init, and handle the callbacks.
//may make calls to both the modal and view to trigger data changes.
function aRest_controller(modal, view){


	var controllerInit = function(){
		//trigger initial data and view state
		triggerRefactoring();
		//listen for events to change that state
		$("body").delegate($("."+aRest_helper.optionObject+""), "change", function(){
				//update data
				triggerRefactoring();
		});
	}


	var triggerRefactoring = function(){
		var optionElements = $("."+aRest_helper.optionObject+"");

		$(optionElements).each(function(i){
			//cycle elements, and determine state of each, recording results
			var optionId = aRest_helper.optionIdToJsonId(this.id);
			var status = aRest_helper.formElementActive(this);
			modal.changeStatus(optionId,status);
		});
		//update the visibility based on restraints
		modal.refactorOptions();
		view.updateVisibility();
	}

	//init
	controllerInit();
}




//////////////////////////////////////default theme//////////////////////////////////////////////
//---------------------------------------------------------------------------------------------//
//the theme can be accessed from the object, by object.theme - and it gives the developer full control over how elements are 
//formatted. This decision was made with reuse in mind. If modifications are needed to wrap a module, it can be done by making
//alterations to the prototypes of the following functions.
//default is to xhtml tags for common input elements, and html5 tags for moden complient browsers
//these functions have no knowledge of the internals of the module, but rather are passed information that can be read and formatted

